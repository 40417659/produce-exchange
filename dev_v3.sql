-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for organic_produce
CREATE DATABASE IF NOT EXISTS `dev_v3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dev_v3`;

-- Dumping structure for table organic_produce.login
CREATE TABLE IF NOT EXISTS `login` (
  `PK_email` varchar(45) NOT NULL,
  `password` varchar(250) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`PK_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=1260 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.meal
CREATE TABLE IF NOT EXISTS `meal` (
  `meal_id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_name` varchar(45) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  PRIMARY KEY (`meal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.meal_contents
CREATE TABLE IF NOT EXISTS `meal_contents` (
  `contents_id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight_in_kg` decimal(10,2) NOT NULL,
  PRIMARY KEY (`contents_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.order
CREATE TABLE IF NOT EXISTS `order` (
  `PK_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `customer_email` varchar(45) NOT NULL,
  `supplier_email` varchar(45) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `weight_in_kg` decimal(10,2) NOT NULL,
  `date_ordered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_required` date DEFAULT NULL,
  PRIMARY KEY (`PK_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1062 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.predicted_stock
CREATE TABLE IF NOT EXISTS `predicted_stock` (
  `PK_predicted_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight_in_kg` decimal(10,2) NOT NULL,
  `date_predicted` date NOT NULL,
  PRIMARY KEY (`PK_predicted_id`,`date_predicted`)
) ENGINE=InnoDB AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=8192 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.product
CREATE TABLE IF NOT EXISTS `product` (
  `PK_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `variant` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price_per_kg` decimal(10,2) NOT NULL,
  `season` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PK_product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1063 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=321 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.request
CREATE TABLE IF NOT EXISTS `request` (
  `PK_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  `weight_in_kg` decimal(10,2) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `price_at_time` decimal(10,2) NOT NULL,
  `fulfilled` int(1) DEFAULT NULL,
  PRIMARY KEY (`PK_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1042 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=16384 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `PK_review_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(45) NOT NULL,
  `rating` int(1) NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`PK_review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.specialist
CREATE TABLE IF NOT EXISTS `specialist` (
  `specialist_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(45) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`specialist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1011 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=5461 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.supplier_stock
CREATE TABLE IF NOT EXISTS `supplier_stock` (
  `PK_supplier_stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(45) NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight_in_kg` decimal(10,2) NOT NULL,
  PRIMARY KEY (`PK_supplier_stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030 DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2730 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.user
CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `business_name` varchar(45) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  `approved` int(1) DEFAULT NULL,
  `specialist` int(1) DEFAULT NULL,
  `user_type` int(1) NOT NULL,
  `cert_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=3276 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

-- Dumping structure for table organic_produce.user_auth
CREATE TABLE IF NOT EXISTS `user_auth` (
  `email` varchar(45) NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=2340 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
