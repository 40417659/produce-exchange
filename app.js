/**
 * Node.js backend for OpenProduceExchange Project.
 * Date Created 26/05/2020
 */
var SQL_IPADDRESS = "127.0.0.1"

const fs = require("fs");

var mysql = require('mysql');
var express = require('express');
var expressSesssion = require('express-session');
var path = require('path');
var bodyParser = require("body-parser");
var moment = require("moment");
var nodemailer = require("nodemailer"); // npm install nodemailer
var generator = require("generate-password"); // Password generator (npm install generate-password --save)
var sha256 = require("js-sha256")
const cron = require("node-cron");
const { updateLocale } = require('moment');
const { format } = require("path");
const { response } = require("express");

var app = express();

// EM: organicproduceexchange@gmail.com
// PW: 5=FZ,9JxRk

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'organicproduceexchange@gmail.com',
        pass: '5=FZ,9JxRk'
    }
});

var connection = mysql.createConnection({
    // MySQL Connection details.
    host: SQL_IPADDRESS,
    user: "root",
    password: "password",
    database: "dev_v3"
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static(__dirname))

app.get("/", function (request, response) {
    response.sendFile(path.join(__dirname + "/index.html"));
});

app.get("/customer", function (request, response) {
    response.sendFile(path.join(__dirname + "/customer.html"));
})

app.get("/supplier", function (request, response) {
    response.sendFile(path.join(__dirname + "/supplier.html"));
})

app.get("/admin", function (request, response) {
    response.sendFile(path.join(__dirname + "/admin.html"))
})

app.get("/products", function (request, response) {
    connection.query("SELECT * FROM product", function (error, results, fields) {
        response.send(results);
    })
});

//Dont think I need this
app.get("/viewproduct", function (request, response) {
    var product = request.body.product_id;
    connection.query("SELECT * FROM supplier_stock WHERE product_id = ?", [product], function (error, results, fields) {
        if (results.length > 0) {
            // return product data
            response.send("product found");
        } else {
            response.send("No product found " + product_id);
        }
        response.end();
    });
});


app.post("/forgotPass", function (request, response) {

    var email = request.body.email;

    var passwd = generator.generate({
        length: 8,
        numbers: true
    });

    connection.query("UPDATE `user` SET `password` = ? WHERE `email` = ?", [passwd, email], function (error, results, fields) {
        console.log("Updated pass to : " + passwd);

        var mailOptions = {
            from: 'organicproduceexchange@gmail.com',
            to: email,
            subject: "OPE: Password Reset Request",
            text: "Here is your new password, please change this when logging in!\nPassword: " + passwd
        }

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                response.send("Mail failed.");
                response.end();
                console.log("Error: " + error);
            } else {
                response.send("Mail sent!");
                response.end();
                console.log("Email sent: " + info.response);
            }
        });


    });


});

app.post("/updateUser", function (request, response) {

    var body = request.body;

    var hash = body.hash;
    var email = body.email;
    var name = body.fname + " " + body.lname;
    var phone = body.phoneNo;
    var address = body.addressLine1 + ", " + body.addressLine2 + ", " + body.postcode + ", " + body.city + ", " + body.county;
    var businessName = body.businessName;

    if (!checkPostcode(body.postcode)) {
        response.send("Address postcode is invalid");
        response.end();
        return;
    }
    if (!checkEmail(email)) {
        response.send("Email is invalid");
        response.end();
        return;
    }
    if (!checkName(name)) {
        response.send("Name is invalid");
        response.end();
        return;
    }
    if (!checkPhone(phone)) {
        response.send("Phone number is invalid");
        response.end();
        return;
    }

    connection.query("UPDATE `user` SET name = ?, business_name = ?, phone_number = ?, address = ?, email = ? WHERE `email` = ?", [name, businessName, phone, address, email, email], function (error, results, fields) {
        console.log("Updated user.");
        console.log("Errors: " + error);
    });


});

// Works
app.post("/supplierStock", function (request, response) {
    //Uses the hash to check if a valid email exists
    //If exists sets autheticated to true, otherwise to false
    //Could turn into a seperate function for checking hash as would be used elsewhere
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [request.body.hash], function (error, results, fields) {
        if (results.length > 0) { //Authentication succeeds
            console.log("Authenticated");
            var email = results[0].email //The users email
            connection.query("SELECT user_type FROM user JOIN user_auth ON user.email = user_auth.email WHERE user.email = ?", [email], function (error, results, fields) {
                if (results[0]["user_type"] == 3) {
                    response.send({ "supplier": false })
                    return;
                }
                //Shows all the suppliers stock
                connection.query("SELECT product.type, supplier_stock.weight_in_kg, product.name, product.variant FROM supplier_stock INNER JOIN product ON supplier_stock.product_id = product.PK_product_id WHERE supplier_stock.user_email =?", [email], function (error, results, fields) {
                    if (results.length > 0) {
                        response.send(results);
                    } else {
                        response.send({"No Stock": true});
                    }
                });
            });
            //Authentication fails      
        } else {
            console.log("unauthorized");
            authenticated = false;
            response.send({ "authenticated": false })
            return;
        }
    });
});

app.post("/predictedStock", function(request, response) {
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [request.body.hash], function (error, results, fields) {
        if (results.length > 0) {
            var email = results[0]["email"];
            connection.query("SELECT product.name, predicted_stock.email, predicted_stock.weight_in_kg, predicted_stock.product_id, predicted_stock.date_predicted FROM predicted_stock JOIN product on predicted_stock.product_id = product.PK_product_id WHERE email = ?",[email], function(error, results, fields) {
                response.send(results);
            });
        } else {
            console.log("unauthorized");
            authenticated = false;
            response.send({
                "authenticated": false
            })
            return;
        }
    });
});

app.post("/addStock", function (request, response) {
    var authenticated = false;
    console.log(request.body);
    //Test values, would be taken from the user input
    //Uses the hash to check if a valid email exists
    //If exists sets autheticated to true, otherwise to false
    //Could turn into a seperate function for checking hash as would be used elsewhere
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [request.body.hash], function (error, results, fields) {
        if (results.length > 0) { //Authentication succeeds
            console.log("Authenticated");
            var email = results[0].email //The users email
            var productId = request.body.id;
            var weightInKg = Number(request.body.quantity);
            //console.log(request.body);
            //console.log(productId);
            //console.log(weightInKg);

            //Checks if record exists
            var now = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            var orderDate = new Date(request.body.date);
            orderDate.setHours(orderDate.getHours() - 1);
            if (now < orderDate) {
                connection.query("SELECT PK_predicted_id, weight_in_kg, date_predicted FROM predicted_stock WHERE email = ? AND product_id = ?", [email, productId], function (error, results, fields) {
                    if (results.length > 0) {
                        dbDate = new Date(results[0]["date_predicted"])
                        if (dbDate.getTime() == orderDate.getTime()) {
                            console.log("here");
                            var stockId = results[0].PK_predicted_id; //Gets unique identifer from stock table
                            var originalWeight = results[0].weight_in_kg; //Gets weight from db
                            var newWeight = originalWeight + weightInKg; //Calculates new weight
                            console.log(stockId + " " + originalWeight + " " + newWeight)
                            connection.query("UPDATE predicted_stock SET weight_in_kg = ? WHERE PK_predicted_id = ? AND product_id = ?", [newWeight, stockId, productId], function (error, results, fields) {
                                if (error == null) {
                                    console.log("updated");
                                    response.send({
                                        "stock": true
                                    })
                                } else { // Error, most likely email doesn't exist in login
                                    console.log(error);
                                    console.log("failed to update");
                                    response.send({ "stock": false })
                                    return;
                                }
                            })
                        } else {
                            connection.query("INSERT INTO `predicted_stock` (`email`, `product_id`, `weight_in_kg`, `date_predicted`) VALUES (?, ?, ?, ?)", [email, productId, weightInKg, orderDate], function (error, results, fields) {
                                if (error == null) {
                                    console.log("inserted");
                                    response.send({
                                        "stock": true
                                    })
                                } else { // Error, most likely email doesn't exist in login
                                    console.log(error);
                                    console.log("failed to insert");
                                    response.send({
                                        "stock": false
                                    })
                                    return;
                                }
                            });
                        }
                    } else {
                        connection.query("INSERT INTO `predicted_stock` (`email`, `product_id`, `weight_in_kg`, `date_predicted`) VALUES (?, ?, ?, ?)", [email, productId, weightInKg, orderDate], function (error, results, fields) {
                            if (error == null) {
                                console.log("inserted");
                                response.send({
                                    "stock": true
                                })
                            } else { // Error, most likely email doesn't exist in login
                                console.log(error);
                                console.log("failed to insert");
                                response.send({
                                    "stock": false
                                })
                                return;
                            }
                        });
                    }
                })
            } else {
                connection.query("SELECT PK_supplier_stock_id, weight_in_kg FROM supplier_stock WHERE user_email = ? AND product_id = ?", [email, productId], function (error, results, fields) {
                    if (results.length > 0) {
                        var stockId = results[0].PK_supplier_stock_id; //Gets unique identifer from stock table
                        var originalWeight = results[0].weight_in_kg; //Gets weight from db
                        var newWeight = originalWeight + weightInKg; //Calculates new weight

                        //Updates record with new weight if exists in db already
                        connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [newWeight, stockId, productId], function (error, results, fields) {
                            if (error == null) {
                                console.log("updated");
                                response.send({
                                    "stock": true
                                })
                            } else { // Error, most likely email doesn't exist in login
                                console.log(error);
                                console.log("failed to update");
                                response.send({
                                    "stock": false
                                })
                                return;
                            }
                        });
                        //That product and email doesn't exist in the db
                    } else {
                        //Inserts into supplier stock since record doesn't exist
                        connection.query("INSERT INTO `supplier_stock` (`user_email`, `product_id`, `weight_in_kg`) VALUES (?, ?, ?)", [email, productId, weightInKg], function (error, results, fields) {
                            if (error == null) {
                                console.log("inserted");
                                response.send({
                                    "stock": true
                                })
                            } else { // Error, most likely email doesn't exist in login
                                console.log(error);
                                console.log("failed to insert");
                                response.send({
                                    "stock": false
                                })
                                return;
                            }
                        });
                    }
                });
            }
            //Authentication fails    
        } else {
            console.log("unauthorized");
            authenticated = false;
            response.send({
                "authenticated": false
            })
            return;
        }
    });
});


//Done
app.post("/userInfo", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            email = results[0]["email"]
            connection.query("SELECT * FROM user WHERE email = ?", [email], function (error, results, fields) {
                if (results.length > 0) {
                    response.send(results[0]);
                } else {
                    response.send("User not found")
                }
            })
        } else {
            response.send({ "authenticated": false });
        }
    })
});




app.post("/order", function (request, response) {
    var product = request.body.id; //Product id from webpage
    var quantity = request.body.quantity; //Weight from webpage - requested
    var price; //Price of product
    var requestId; //Primary key of request
    var kgTwoThird = Math.round(quantity * 0.67); //two thirds of the product requested
    var kgOneThird = Math.round(quantity - kgTwoThird); //one third of the product requested
    var weightLeft; //Weight needed
    var specalistFound = false;
    var supplierFound = false;
    var supplierEmail;
    var stockId;//the id of the stock for the product
    var stockWeight;//amount supplier has
    var orderWeight; //Weight placed in order
    var continueSupplier = false;
    var customerEmail;
    var noSpec = true;

    if (quantity <= 0) {
        response.send("Quantity cannot be 0 or lower");
        response.end();
        return;
    }

    //SQL to get the customers email
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [request.body.hash], function (error, results, fields) {
        customerEmail = results[0].email //The users email

        //SQL to get price of produce
        connection.query("SELECT `price_per_kg` FROM `product` WHERE `PK_product_id` = ?", [product], function (error, results, fields) {
            price = results[0]["price_per_kg"]

            //SQL to create a new request
            connection.query("INSERT INTO request (product_id, user_email, weight_in_kg, date, price_at_time) VALUES (?, ?, ?, ?, ?)", [product, customerEmail, quantity, null, price], function (error, results, fields) {
                requestId = results.insertId; //Primary key of request that was just inserted

                //SQL to select all specalists who have stock     
                connection.query("SELECT supplier_stock.* FROM supplier_stock, specialist WHERE supplier_stock.product_id = ? AND supplier_stock.user_email IN (SELECT specialist.user_email FROM specialist WHERE specialist.product_id = ?) GROUP BY supplier_stock.user_email", [product, product], function (error, specalistWithStock, fields) {
                    if (specalistWithStock.length > 0) {
                        noSpec = false;


                    } else {
                        noSpec = true;
                    }

                    if (noSpec == false) {
                        //SQL to get amount of orders
                        connection.query("SELECT COUNT(`order`.supplier_email) AS expr1, `order`.supplier_email FROM `order` GROUP BY `order`.supplier_email ORDER BY expr1", function (error, supplierOrderAmount, fields) {

                            //Loops through all the specalists with stock seeing if they have any orders
                            for (i = 0; i < specalistWithStock.length; i++) {
                                //Checks to see if they have any orders
                                var hasOrders = supplierOrderAmount.some(item => item.supplier_email === specalistWithStock[i]['user_email']);

                                //They have no orders
                                if (hasOrders == false) {
                                    supplierEmail = specalistWithStock[i]['user_email'];
                                    stockId = specalistWithStock[i]['PK_supplier_stock_id'];
                                    stockWeight = specalistWithStock[i]['weight_in_kg'];
                                    specalistFound = true;
                                }
                            }

                            //No orders so give to this specalist
                            if (specalistFound == true) {
                                weightCalcSecond();

                                //All have orders so selects the one with the least amount of orders. First one from results has least.
                            } else {
                                supplierEmail = specalistWithStock[0]['user_email'];

                                if (!checkEmail(supplierEmail)) {
                                    response.send("Supplier email is invalid");
                                    response.end();
                                    return;
                                }

                                connection.query("SELECT PK_supplier_stock_id, weight_in_kg FROM supplier_stock WHERE user_email = ? AND product_id = ?", [supplierEmail, product], function (error, results, fields) {
                                    stockId = results[0]['PK_supplier_stock_id'];
                                    stockWeight = results[0]['weight_in_kg'];
                                    weightCalcSecond();
                                });
                            }

                            function weightCalcSecond() {
                                if (stockWeight > kgTwoThird) {
                                    //can fulfil, has more
                                    stockWeight = stockWeight - kgTwoThird;
                                    orderWeight = kgTwoThird;
                                    weightLeft = kgOneThird;


                                } else if (stockWeight == kgTwoThird) {
                                    //can fulfil, has correct amount but sets its own stock to 0
                                    stockWeight = 0;
                                    orderWeight = kgTwoThird;
                                    weightLeft = kgOneThird;

                                } else if (stockWeight < kgTwoThird) {
                                    //doesn't have enough. add left over to total and set own stock to 0
                                    weightLeft = (kgTwoThird - stockWeight) + kgOneThird;
                                    orderWeight = stockWeight;
                                    stockWeight = 0;

                                }

                                connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                    connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                    });
                                });

                                var totalPrice = price * orderWeight;
                                connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                });

                                if (weightLeft <= 0) {
                                    console.log("Order is 1kg, no split");
                                    connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                        console.log(error);
                                    });

                                } else {
                                    pickSuppliers(weightLeft);
                                }
                            }

                        });//SELECT order count

                    } else if (noSpec == true) {
                        console.log("No specalists with stock");
                        weightLeft = kgTwoThird + kgOneThird;
                        pickSuppliers(weightLeft);
                    }
                });
            });//INSERT request
        });//SELECT price
    });//SELECT auth

    function pickSuppliers(weightLeft) {
        connection.query("SELECT supplier_stock.* FROM supplier_stock, specialist WHERE supplier_stock.product_id = ? AND supplier_stock.user_email NOT IN (SELECT specialist.user_email FROM specialist WHERE specialist.product_id = ?) GROUP BY supplier_stock.user_email", [product, product], function (error, withStockSuppliers, fields) {
            if (withStockSuppliers.length > 0) {
                connection.query("SELECT COUNT(`order`.supplier_email) AS expr1, `order`.supplier_email FROM `order` GROUP BY `order`.supplier_email ORDER BY expr1", function (error, supplierOrderAmount, fields) {
                    //Loops through all the specalists with stock seeing if they have any orders
                    for (i = 0; i < withStockSuppliers.length; i++) {
                        //Checks to see if they have any orders
                        var hasOrders = supplierOrderAmount.some(item => item.supplier_email === withStockSuppliers[i]['user_email']);

                        //They have no orders
                        if (hasOrders == false) {
                            supplierEmail = withStockSuppliers[i]['user_email'];
                            stockId = withStockSuppliers[i]['PK_supplier_stock_id'];
                            stockWeight = withStockSuppliers[i]['weight_in_kg'];
                            supplierFound = true;
                        }
                    }
                    //No orders so give to this specalist
                    if (supplierFound == true) {
                        weightCalc();

                        //All have orders so selects the one with the least amount of orders. First one from results has least.
                    } else {
                        supplierEmail = withStockSuppliers[0]['user_email'];
                        connection.query("SELECT PK_supplier_stock_id, weight_in_kg FROM supplier_stock WHERE user_email = ? AND product_id = ?", [supplierEmail, product], function (error, results, fields) {
                            stockId = results[0]['PK_supplier_stock_id'];
                            stockWeight = results[0]['weight_in_kg'];
                            weightCalc();

                        });
                    }

                    function weightCalc() {
                        if (stockWeight > weightLeft) {
                            //can fulfil, has more
                            stockWeight = stockWeight - weightLeft;
                            orderWeight = weightLeft;
                            weightLeft = 0;

                        } else if (stockWeight == weightLeft) {
                            //can fulfil, has correct amount but sets its own stock to 0
                            stockWeight = 0;
                            orderWeight = weightLeft;
                            weightLeft = 0;

                        } else if (stockWeight < weightLeft) {
                            //doesn't have enough. add left over to total and set own stock to 0
                            weightLeft = weightLeft - stockWeight;
                            orderWeight = stockWeight;
                            stockWeight = 0;

                        }
                        connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                            connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                            });
                        });

                        var totalPrice = price * orderWeight;
                        connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                        });

                        if (weightLeft <= 0) {
                            console.log("Completed");
                            connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                console.log("fulfilled updated")
                                console.log(error);
                            });
                        } else {
                            //console.log("Not completed: " + weightLeft);
                            connection.query("SELECT supplier_stock.* FROM supplier_stock WHERE supplier_stock.product_id = ? AND supplier_stock.weight_in_kg >= ? ORDER BY RAND() LIMIT 1", [product, weightLeft], function (error, results, fields) {
                                if (results.length > 0) {
                                    supplierEmail = results[0]['user_email'];
                                    stockId = results[0]['PK_supplier_stock_id'];
                                    stockWeight = results[0]['weight_in_kg'];
                                    stockWeight = stockWeight - weightLeft;
                                    orderWeight = weightLeft;

                                    connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                        connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                        });
                                        var totalPrice = price * orderWeight;
                                        connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                        });
                                    });

                                    //console.log("done");
                                    connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                        //console.log("fulfilled updated")
                                        //console.log(error);
                                    });
                                } else {
                                    //console.log("No more suppliers with stock? Reduce request kg and then when stock is added to the website, do this search.")
                                    connection.query("UPDATE request SET fulfilled = ?, weight_in_kg = ? WHERE PK_request_id = ?", [0, weightLeft, requestId], function (error, results, fields) {
                                        //console.log("Weight and fulfilled updated")
                                        //console.log(error);
                                    });
                                }
                            });

                        }
                    }
                });
                //If no non-specalist but specalist, will look for anyone with stock
            } else {
                //console.log("NO ONE CAN FULFIL UPDATE FLAG")
                if (weightLeft != 0) {
                    connection.query("SELECT supplier_stock.* FROM supplier_stock WHERE supplier_stock.product_id = ? AND supplier_stock.weight_in_kg >= ? ORDER BY RAND() LIMIT 1", [product, weightLeft], function (error, results, fields) {
                        if (results.length > 0) {
                            supplierEmail = results[0]['user_email'];
                            stockId = results[0]['PK_supplier_stock_id'];
                            stockWeight = results[0]['weight_in_kg'];
                            stockWeight = stockWeight - weightLeft;
                            orderWeight = weightLeft;

                            connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                });
                                var totalPrice = price * orderWeight;
                                connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                });

                            });

                            //console.log("done");
                            connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                //console.log("Fulfilled updated")
                                //console.log(error);
                            });
                        } else {
                            //console.log("No more suppliers with stock? Reduce request kg and then when stock is added to the website, do a search.")
                            connection.query("UPDATE request SET fulfilled = ?, weight_in_kg = ? WHERE PK_request_id = ?", [0, weightLeft, requestId], function (error, results, fields) {
                                //console.log("Weight and fulfilled updated")
                                //console.log(error);
                            });
                        }
                    });

                }
            }
        });
    }
    response.send({"done": true});
});

app.post("/customerRequests", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT user.email, user.user_type, user_auth.hash FROM user JOIN user_auth on user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results[0]["user_type"] == 2) {
            response.send({ "supplier": true })
            return;
        }
        if (results.length > 0) {
            email = results[0]["email"]
            connection.query("SELECT PK_request_id, product_id, name,weight_in_kg,date,price_per_kg,fulfilled FROM request JOIN product ON request.product_id = product.PK_product_id WHERE user_email = ?", [email], function (error, results, fields) {
                response.send(results);
            });
        } else {
            response.send({ "authenticated": false });
        }
    });
})

app.post("/customerOrders", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT user.email, user.user_type, user_auth.hash FROM user JOIN user_auth on user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results[0]["user_type"] == 2) {
            email = results[0]["email"]
            connection.query("SELECT PK_order_id, customer_email, request_id, name, supplier_email, order.weight_in_kg, order.total_price, date_required FROM `order` JOIN request ON order.request_id = request.PK_request_id JOIN product ON request.product_id = product.PK_product_id WHERE supplier_email = ?", [email], function (error, results, fields) {
                if(results.length > 0) {
                    console.log(results)
                    results[0]["supplier"] = true;
                    response.send(results);
                    return;
                }
            });
        } 
        else if (results.length > 0) {
            email = results[0]["email"]
            connection.query("SELECT PK_order_id, customer_email, request_id, name, supplier_email, order.weight_in_kg, order.total_price, date_required FROM `order` JOIN request ON order.request_id = request.PK_request_id JOIN product ON request.product_id = product.PK_product_id WHERE customer_email = ?", [email], function (error, results, fields) {
                response.send(results);
                return;
            });
        } else {
            response.send({ "authenticated": false });
            return;
        }
    });
})

//Done
app.post("/login", function (request, response) {
    var email = request.body.email;
    var password = request.body.password;
    hashPassword = sha256(password)
    connection.query("SELECT * FROM login WHERE PK_email = ? AND password = ?", [email, hashPassword], function (error, results, fields) {
        if (results.length > 0) {
            console.log("Login Successful");
            var hash = sha256(email + "::" + password)
            connection.query("SELECT user_type FROM user WHERE email = ?", [email], function (error, results, fields) {
                response.send({
                    "user_type": results[0]["user_type"],
                    "login": true,
                    "hash": hash
                })
            })
        } else {
            response.send({
                "login": false
            })
            console.log("Incorrect email or password");
        }
    });

});

//Change due to supplier table 
app.post('/signup', function (request, response) {
    // Function to handle signup data sent to "./signup".

    var email = request.body.email;
    var password = request.body.password;
    var phone = request.body.phoneNo;
    var business = request.body.business;
    var name = request.body.name;
    var supplier = request.body.role;
    var address = request.body.address;
    var orgCert = request.body.orgCert;
    password = sha256(password)

    connection.query("SELECT PK_email FROM login WHERE PK_email = ?", [email], function (error, results, fields) {
        if (results.length > 0) {
            response.send({ "exists": true, "successful": false });
            return;
        } else {
            //Create account
            connection.query("INSERT INTO login (PK_email, password, create_time) VALUES(?, ?, ?)", [email, password, moment(Date.now()).format('YYYY-MM-DD HH:mm:ss')], function (error, results, fields) {
                if (error === null) {
                    if (supplier == true) {
                        connection.query("INSERT INTO user (email, name, business_name, phone_number, address, user_type, cert_code) VALUES (?, ?, ?, ?, ?, ?, ?)", [email, name, business, phone, address, 2, orgCert], function (error, results, fields) {
                            if (error === null) {
                                var hash = sha256(email + "::" + password)
                                connection.query("INSERT INTO user_auth (email, hash) VALUES (?, ?)", [email, hash], function (error, results, fields) {
                                    console.log("here");
                                    console.log(error)
                                    response.send({ "exists": false, "successful": true });
                                    return;
                                });
                            }
                        });
                    } else {
                        connection.query("INSERT INTO user (email, name, business_name, phone_number, address, user_type) VALUES (?, ?, ?, ?, ?, ?)", [email, name, business, phone, address, 3], function (error, results, fields) {
                            if (error === null) {
                                var hash = sha256(email + "::" + password)
                                connection.query("INSERT INTO user_auth (email, hash) VALUES (?, ?)", [email, hash], function (error, results, fields) {
                                    console.log(error)
                                    response.send({ "exists": false, "successful": true });
                                    return;
                                });
                            }
                        });
                    }
                }
            });
        }
    });
});


app.post("/getStock", function(request, response){
    var hash = request.body.hash;
    connection.query("SELECT user.email, user_auth.hash, user_type FROM user_auth JOIN user ON user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            if (results[0]["user_type"] != 1) {
                console.log("Unauthorised Admin")
                if (results[0]["user_type"] == 2) {
                    response.send({ "supplier": true })
                    return;
                }
                if (results[0]["user_type"] == 3) {
                    response.send({ "supplier": false })
                    return;
                }
            }
            connection.query("SELECT user.email, user.name, user.business_name, product.name AS product_name, supplier_stock.weight_in_kg FROM supplier_stock JOIN product ON supplier_stock.product_id = product.PK_product_id JOIN user ON supplier_stock.user_email = user.email", function (error, results, fields) {
                response.send(results);
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});


app.post("/getCustomers", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT user.email, user_auth.hash, user_type FROM user_auth JOIN user ON user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            if (results[0]["user_type"] != 1) {
                console.log("Unauthorised Admin")
                if (results[0]["user_type"] == 2) {
                    response.send({ "supplier": true })
                    return;
                }
                if (results[0]["user_type"] == 3) {
                    response.send({ "supplier": false })
                    return;
                }
            }
            connection.query("SELECT * FROM user WHERE user_type = 3", function (error, results, fields) {
                response.send(results);
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});

app.post("/getSuppliers", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT user.email, user_auth.hash, user_type FROM user_auth JOIN user ON user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            if (results[0]["user_type"] != 1) {
                console.log("Unauthorised Admin")
                if (results[0]["user_type"] == 2) {
                    response.send({ "supplier": true })
                    return;
                }
                if (results[0]["user_type"] == 3) {
                    response.send({ "supplier": false })
                    return;
                }
            }
            connection.query("SELECT * FROM user WHERE user_type = 2", function (error, results, fields) {
                response.send(results);
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});

app.post("/getOrders", function (request, response) {
    var hash = request.body.hash;
    connection.query("SELECT user.email, user_auth.hash, user_type FROM user_auth JOIN user ON user.email = user_auth.email WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            if (results[0]["user_type"] != 1) {
                console.log("Unauthorised Admin")
                if (results[0]["user_type"] == 2) {
                    response.send({ "supplier": true })
                    return;
                }
                if (results[0]["user_type"] == 3) {
                    response.send({ "supplier": false })
                    return;
                }
            }
            connection.query("SELECT PK_order_id, request_id, customer_email, supplier_email, request.product_id, `order`.weight_in_kg, total_price, date_ordered, date_required FROM `order` JOIN request ON request.PK_request_id = `order`.request_id", function (error, results, fields) {
                response.send(results);
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});

app.post("/deleteUser", function (request, response) {
    var hash = request.body.hash;
    var email = request.body.email;
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            connection.query("DELETE FROM login WHERE PK_email = ?", [email], function (error, results, fields) { 
                console.log(results);
                response.send({"deleted": true});
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});

app.post("/approveUser", function (request, response) {
    var hash = request.body.hash;
    var email = request.body.email;
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            connection.query("UPDATE user SET approved = 1 WHERE email = ?", [email], function (error, results, fields) { 
                response.send({"approved": true})
            });
        } else {
            response.send({ "authenticated": false })
        }
    });
});

app.post("/saveMeal", function(request, response) {
    var hash = request.body[0]["hash"];
    var name = request.body[0]["name"];
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            var email = results[0].email
            connection.query("INSERT INTO meal (meal_name, user_email) VALUES(?,?)",[name, email], function(error, results, fields) {
                if(error == null) {
                    for(var i = 1; i < request.body.length; i++) {
                        var current = request.body[i];
                        connection.query("INSERT INTO meal_contents (meal_id, product_id, weight_in_kg) VALUES(?,?,?)",[results["insertId"], current["id"], current["quantity"]], function(error, results, fields) {
                            if(error != null) {
                                console.log(error);
                                response.send({ "successful": false });
                                return;
                            }
                        });
                    }
                    response.send({ "successful": true });
                    return;
                }
            });
        } else {
            console.log("or here")
            response.send({ "authenticated": false })
            return;
        }
    });
});

app.post("/meals", function(request, response) {
    var hash = request.body.hash;
    var payload = [];
    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            var email = results[0]["email"];
            connection.query("SELECT * FROM meal JOIN meal_contents ON meal.meal_id = meal_contents.meal_id WHERE user_email = ?",[email], function(error, results, fields) {
                if(results.length > 0) {
                    console.log(results)
                    response.send(results);
                }
            });

        } else {
            console.log("or here")
            response.send({ "authenticated": false })
            return;
        }
    });
});

function checkEmail(email) {
    return true;
    var emailPatt = '[\w\.\-]*@[\w\.\-]*\w';
    return emailPatt.test(email);
}
function checkPhone(phone) {
    return true;
    var phonePatt = '\+?\d?\d?[\-\s(]?0?\)?\s?\d{3,4}[\s\-]?\d{3}[\s\-]?\d{3,4}';
    return phonePatt.test(phone);
}

function checkName(firstName, lastName) {
    return true;
    //regex checks each name has only letters and a length of greater than 1
    var namePatt = '^[A-z]{2}[A-z]*$';
    if (namePatt.test(firstName) && namePatt.test(lastName)) {
        return true;
    }
    return false;
}
function checkPassword(password) {
    if (password.length >= 8) {
        return true;
    }
    return false;
}
function checkPostcode(postcode) {
    return true;
    //checks for valid UK postcode, does not support overseas territories
    var postcodePatt = '^[A-Z]?[A-Z][0-9][A-Z0-9]? [0-9][A-Z]{2}$';
    return postcode.test(postcode)
}

app.post("/editInfo", function (request, response) {
    var address = request.body.addressLine1 + ", " + request.body.addressLine2 + ", " + request.body.postcode + ", " + request.body.city + ", " + request.body.county;
    var phone = request.body.phoneNo;
    var business = request.body.businessName;
    var name = request.body.fname + " " + request.body.lname;
    var hash = request.body.hash;

    connection.query("SELECT email FROM user_auth WHERE hash = ?", [hash], function (error, results, fields) {
        if (results.length > 0) {
            connection.query("UPDATE user SET name = ?, business_name = ?, phone_number = ?, address = ? WHERE email = ?", [name, business, phone, address, results[0]["email"]], function (error, results, fields) {
                if (error == null) {
                    response.send({ "successful": true });
                } else {
                    console.log(error);
                    response.send({ "successful": false });
                }
            });

        }
    });

});

//Predicted stock check at 01:00 of everyday
cron.schedule("0 1 * * *", function () {
    var dateToday = moment(Date.now()).format('YYYY-MM-DD')
    connection.query("SELECT * FROM predicted_stock WHERE date_predicted = ?", [dateToday], function(error, results, fields){
        if(results.length > 0){
            for (var i = 0; i < results.length; i++) {
                var email = results[i]['email'];
                var productId = results[i]['product_id'];
                var weight = results[i]['weight_in_kg'];
                var id = results[i]['PK_predicted_id'];
                checkExist(email, productId, weight, id);
            } 
        } else {
            //console.log("None on this date");
        }
    });
    //See if there is stock already in supplier_stock
    function checkExist(email, productId, weight, id){
        connection.query("SELECT * FROM supplier_stock WHERE user_email = ? AND product_id = ?",[email, productId], function(error, resultsStock, fields){
            if(resultsStock.length > 0){
               weight = weight + resultsStock[0]['weight_in_kg'];
               updateStock(email, productId, weight, id);
            } else {
                insertStock(email, productId, weight, id);
            }
        });
    }
    //Updates the stock in supplier_stock
    function updateStock(email, productId, weight, id){
        connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE user_email = ? AND product_id = ?", [weight, email, productId ], function (error, results, fields) {
            deletePredicted(id);
        });
    }
    //Creates new stock in supplier_stock
    function insertStock(email, productId, weight, id){
        connection.query("INSERT INTO supplier_stock (user_email, product_id, weight_in_kg) VALUES (?, ?, ?)", [email, productId, weight], function (error, results, fields) {
            deletePredicted(id);
        });
    }
    //Deletes predicted stock
    function deletePredicted(id){
        connection.query("DELETE FROM predicted_stock WHERE PK_predicted_id = ?",[id], function(error, results, fields){
        });
    }
});

cron.schedule("* * * * *", function () {
    console.log("running a task every minute");
    connection.query("SELECT * FROM request WHERE fulfilled = ? ORDER BY RAND() LIMIT 1", [0], function (error, requestsResults, fields) {

        if (requestsResults.length > 0) {
            //console.log("Open requests");
            //for (var i = 0; i < requestsResults.length; i++) {
            var requestId = requestsResults[0]['PK_request_id']
            var product = requestsResults[0]['product_id']
            var customerEmail = requestsResults[0]['user_email']
            var quantity = requestsResults[0]['weight_in_kg']
            var price = requestsResults[0]['price_at_time']

            //createOrders(requestId, product, customerEmail, quantity, price);
            //console.log(i + " : done");

            // function createOrders(requestId, product, customerEmail, quantity, price) {

            var kgTwoThird = Math.round(quantity * 0.67); //two thirds of the product requested
            var kgOneThird = Math.round(quantity - kgTwoThird); //one third of the product requested
            var kgTwoThird;
            var kgOneThird;
            var weightLeft; //Weight needed
            var specalistFound = false;
            var supplierFound = false;
            var supplierEmail;
            var stockId;//the id of the stock for the product
            var stockWeight;//amount supplier has
            var orderWeight; //Weight placed in order
            var continueSupplier = false;
            var customerEmail;
            var noSpec = true;


            //SQL to select all specalists who have stock     
            connection.query("SELECT supplier_stock.* FROM supplier_stock, specialist WHERE supplier_stock.product_id = ? AND supplier_stock.user_email IN (SELECT specialist.user_email FROM specialist WHERE specialist.product_id = ?) GROUP BY supplier_stock.user_email", [product, product], function (error, specalistWithStock, fields) {
                //console.log(errror);
                if (specalistWithStock.length > 0) {
                    noSpec = false;


                } else {
                    noSpec = true;
                }

                if (noSpec == false) {
                    //SQL to get amount of orders
                    connection.query("SELECT COUNT(`order`.supplier_email) AS expr1, `order`.supplier_email FROM `order` GROUP BY `order`.supplier_email ORDER BY expr1", function (error, supplierOrderAmount, fields) {

                        //Loops through all the specalists with stock seeing if they have any orders
                        for (i = 0; i < specalistWithStock.length; i++) {
                            //Checks to see if they have any orders
                            var hasOrders = supplierOrderAmount.some(item => item.supplier_email === specalistWithStock[i]['user_email']);

                            //They have no orders
                            if (hasOrders == false) {
                                supplierEmail = specalistWithStock[i]['user_email'];
                                stockId = specalistWithStock[i]['PK_supplier_stock_id'];
                                stockWeight = specalistWithStock[i]['weight_in_kg'];
                                specalistFound = true;
                            }
                        }

                        //No orders so give to this specalist
                        if (specalistFound == true) {
                            weightCalcSecond();

                            //All have orders so selects the one with the least amount of orders. First one from results has least.
                        } else {
                            supplierEmail = specalistWithStock[0]['user_email'];

                            connection.query("SELECT PK_supplier_stock_id, weight_in_kg FROM supplier_stock WHERE user_email = ? AND product_id = ?", [supplierEmail, product], function (error, results, fields) {
                                stockId = results[0]['PK_supplier_stock_id'];
                                stockWeight = results[0]['weight_in_kg'];
                                weightCalcSecond();
                            });
                        }

                        function weightCalcSecond() {
                            if (stockWeight > kgTwoThird) {
                                //can fulfil, has more
                                stockWeight = stockWeight - kgTwoThird;
                                orderWeight = kgTwoThird;
                                weightLeft = kgOneThird;


                            } else if (stockWeight == kgTwoThird) {
                                //can fulfil, has correct amount but sets its own stock to 0
                                stockWeight = 0;
                                orderWeight = kgTwoThird;
                                weightLeft = kgOneThird;

                            } else if (stockWeight < kgTwoThird) {
                                //doesn't have enough. add left over to total and set own stock to 0
                                weightLeft = (kgTwoThird - stockWeight) + kgOneThird;
                                orderWeight = stockWeight;
                                stockWeight = 0;

                            }

                            //console.log(" 1   ID: " + stockId + " NEW STOCK WEIGHT: " + stockWeight + " ORDER WEIGHT: " + orderWeight + " WEIGHT LEFT: " + weightLeft);

                            connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                });
                            });

                            var totalPrice = price * orderWeight;
                            connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                            });

                            if (weightLeft <= 0) {
                                //console.log("Order is 1kg, no split");
                                connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                    // console.log(error);
                                });

                            } else {
                                pickSuppliers(weightLeft);
                            }
                        }

                    });//SELECT order count

                } else if (noSpec == true) {
                    //console.log("No specalists with stock");
                    weightLeft = kgTwoThird + kgOneThird;
                    pickSuppliers(weightLeft);
                }
            });

            function pickSuppliers(weightLeft) {
                connection.query("SELECT supplier_stock.* FROM supplier_stock, specialist WHERE supplier_stock.product_id = ? AND supplier_stock.user_email NOT IN (SELECT specialist.user_email FROM specialist WHERE specialist.product_id = ?) GROUP BY supplier_stock.user_email", [product, product], function (error, withStockSuppliers, fields) {
                    if (withStockSuppliers.length > 0) {

                        connection.query("SELECT COUNT(`order`.supplier_email) AS expr1, `order`.supplier_email FROM `order` GROUP BY `order`.supplier_email ORDER BY expr1", function (error, supplierOrderAmount, fields) {
                            //Loops through all the specalists with stock seeing if they have any orders
                            for (i = 0; i < withStockSuppliers.length; i++) {
                                //Checks to see if they have any orders
                                var hasOrders = supplierOrderAmount.some(item => item.supplier_email === withStockSuppliers[i]['user_email']);

                                //They have no orders
                                if (hasOrders == false) {
                                    supplierEmail = withStockSuppliers[i]['user_email'];
                                    stockId = withStockSuppliers[i]['PK_supplier_stock_id'];
                                    stockWeight = withStockSuppliers[i]['weight_in_kg'];
                                    supplierFound = true;
                                }
                            }
                            //No orders so give to this specalist
                            if (supplierFound == true) {
                                weightCalc();

                                //All have orders so selects the one with the least amount of orders. First one from results has least.
                            } else {
                                supplierEmail = withStockSuppliers[0]['user_email'];
                                connection.query("SELECT PK_supplier_stock_id, weight_in_kg FROM supplier_stock WHERE user_email = ? AND product_id = ?", [supplierEmail, product], function (error, results, fields) {
                                    stockId = results[0]['PK_supplier_stock_id'];
                                    stockWeight = results[0]['weight_in_kg'];
                                    weightCalc();

                                });
                            }

                            function weightCalc() {
                                if (stockWeight > weightLeft) {
                                    //can fulfil, has more
                                    stockWeight = stockWeight - weightLeft;
                                    orderWeight = weightLeft;
                                    weightLeft = 0;

                                } else if (stockWeight == weightLeft) {
                                    //can fulfil, has correct amount but sets its own stock to 0
                                    stockWeight = 0;
                                    orderWeight = weightLeft;
                                    weightLeft = 0;

                                } else if (stockWeight < weightLeft) {
                                    //doesn't have enough. add left over to total and set own stock to 0
                                    weightLeft = weightLeft - stockWeight;
                                    orderWeight = stockWeight;
                                    stockWeight = 0;

                                }
                                //console.log("2   ID: " + stockId + " NEW STOCK WEIGHT: " + stockWeight + " ORDER WEIGHT: " + orderWeight + " WEIGHT LEFT: " + weightLeft);
                                connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                    connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                    });
                                });

                                var totalPrice = price * orderWeight;
                                connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                });

                                if (weightLeft <= 0) {
                                    //console.log("Completed");
                                    connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                        //console.log("fulfilled updated")
                                        //console.log(error);
                                    });
                                } else {
                                    //console.log("Not completed: " + weightLeft);
                                    connection.query("SELECT supplier_stock.* FROM supplier_stock WHERE supplier_stock.product_id = ? AND supplier_stock.weight_in_kg >= ? ORDER BY RAND() LIMIT 1", [product, weightLeft], function (error, results, fields) {
                                        if (results.length > 0) {
                                            supplierEmail = results[0]['user_email'];
                                            stockId = results[0]['PK_supplier_stock_id'];
                                            stockWeight = results[0]['weight_in_kg'];
                                            stockWeight = stockWeight - weightLeft;
                                            orderWeight = weightLeft;
                                            connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                                connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                                });
                                                var totalPrice = price * orderWeight;
                                                connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                                });
                                            });

                                            //console.log("done");
                                            connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                                //console.log("Fulfilled updated id: " + requestId);

                                                //console.log(error);
                                            });
                                        } else {
                                            //console.log("No more suppliers with stock? Reduce request kg and then when stock is added to the website, do this search.")
                                            connection.query("UPDATE request SET fulfilled = ?, weight_in_kg = ? WHERE PK_request_id = ?", [0, weightLeft, requestId], function (error, results, fields) {
                                                //console.log("Weight and fulfilled updated : " + requestId)
                                                //console.log(error);
                                            });
                                        }
                                    });

                                }
                            }
                        });
                        //If no non-specalist but specalist, will look for anyone with stock
                    } else {
                        //console.log("NO ONE CAN FULFIL UPDATE FLAG")
                        if (weightLeft != 0) {
                            connection.query("SELECT supplier_stock.* FROM supplier_stock WHERE supplier_stock.product_id = ? AND supplier_stock.weight_in_kg >= ? ORDER BY RAND() LIMIT 1", [product, weightLeft], function (error, results, fields) {
                                if (results.length > 0) {
                                    supplierEmail = results[0]['user_email'];
                                    stockId = results[0]['PK_supplier_stock_id'];
                                    stockWeight = results[0]['weight_in_kg'];
                                    stockWeight = stockWeight - weightLeft;
                                    orderWeight = weightLeft;

                                    connection.query("UPDATE supplier_stock SET weight_in_kg = ? WHERE PK_supplier_stock_id = ? AND product_id = ?", [stockWeight, stockId, product], function (error, results, fields) {
                                        connection.query("DELETE FROM supplier_stock WHERE weight_in_kg <= 0", function (error, results, fields) {
                                        });
                                        var totalPrice = price * orderWeight;
                                        connection.query("INSERT INTO `order` (request_id, customer_email, supplier_email, total_price, weight_in_kg) VALUES (?, ?, ?, ?,?)", [requestId, customerEmail, supplierEmail, totalPrice, orderWeight], function (error, results, fields) {
                                        });

                                    });

                                    //console.log("done");
                                    connection.query("UPDATE request SET fulfilled = ? WHERE PK_request_id = ?", [1, requestId], function (error, results, fields) {
                                        //console.log("Fulfilled updated id: " + requestId);
                                        //console.log(error);
                                    });
                                } else {
                                    //console.log("No more suppliers with stock? Reduce request kg and then when stock is added to the website, do a search.");
                                    connection.query("UPDATE request SET fulfilled = ?, weight_in_kg = ? WHERE PK_request_id = ?", [0, weightLeft, requestId], function (error, results, fields) {
                                        //console.log("Weight and fulfilled updated : " + requestId);
                                        //console.log(error);
                                    });
                                }
                            });

                        }
                    }
                });
            }


            //}//FOR loop through requests

        } else {
            console.log("No requests found");

        }
    });//SELECT requests not fulfilled
});

app.listen(3000);

