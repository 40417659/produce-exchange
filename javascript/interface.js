function getData(){
    document.getElementById("hiddenID").style.display = "none";
    window.bulkBasket = [];
    window.basketList = [];
    var cookie = document.cookie;
    window.payload = {
        "hash": cookie
    }
    getUserInfo();
    getSupplierStock();
    getCustomerRequests();
    getCustomerOrders();
}

function getSupplierStock() {
    var currentStockRequest = postRequest("supplierStock", payload);
    currentStockRequest.onreadystatechange = function() {
        if(currentStockRequest.readyState == 4 && currentStockRequest.status == 200) {
            if(currentStockRequest.response === "") {return}
            console.log(currentStockRequest.response);
            window.stockData = JSON.parse(currentStockRequest.response)
            if(stockData["supplier"] == false) {return}
            console.log(stockData);
            populateStock();
        } 
    }
}

function getUserInfo() {
    var userInfoRequest = postRequest("userInfo", payload);
    userInfoRequest.onreadystatechange = function() {
        if(userInfoRequest.readyState == 4 && userInfoRequest.status == 200) {
            window.userData = JSON.parse(userInfoRequest.response);
            var welcome = document.createElement("h4");
            var container = document.getElementById("wlcmMesg");
            welcome.innerHTML = "Welcome " + userData["name"];
            welcome.className = "w3-bar-item";
            container.appendChild(welcome);
            if(userData["user_type"] == 2) {
                getPredictedStock();
            } 
            if(userData["user_type"] == 3) {
                getMeals();
            }
            populateInfo();
            getProducts();
        }
    }
}

function getMeals() {
    var xhr = postRequest("meals", payload);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            window.mealData = JSON.parse(xhr.response);
            console.log(mealData)
            populateMeals();
        }
    }
}

function getProducts() {
    var productsRequest = getRequest("products", null);
    productsRequest.onreadystatechange = function() {
        if(productsRequest.readyState == 4 && productsRequest.status == 200) {
            window.productData = JSON.parse(productsRequest.response);
            updateStockType();

        }
    }
}

function getCustomerRequests() {
    var requestsRequest = postRequest("customerRequests", payload);
    requestsRequest.onreadystatechange = function() {
        if(requestsRequest.readyState == 4 && requestsRequest.status == 200) {
            if(requestsRequest.response === "") {return}
            window.requestData = JSON.parse(requestsRequest.response);
            if(requestData["supplier"] == true) {return}
            populateRequests();
        }
    }
}

function getCustomerOrders() {
    var orderRequest = postRequest("customerOrders", payload);
    orderRequest.onreadystatechange = function() {
        if(orderRequest.readyState == 4 && orderRequest.status == 200) {
            if(orderRequest.response === "") {return}
            window.orderData = JSON.parse(orderRequest.response);
            populateOrders();
        }
    }
}

function getPredictedStock() {
    var predictedStockRequest = postRequest("predictedStock", payload);
    predictedStockRequest.onreadystatechange = function() {
        if(predictedStockRequest.readyState == 4 && predictedStockRequest.status == 200) {
            if(predictedStockRequest.response === "") {return};
            window.predictedStockData = JSON.parse(predictedStockRequest.response);
            populatePredictedStock();
        }
    }
}

function requestStock() {
    document.getElementById("stock").hidden = false;
    document.getElementById("mealOrd").hidden = true;
    document.getElementById("orders").hidden = true;
    document.getElementById("accountInfo").hidden = true;
    document.getElementById("requests").hidden = true;
    document.getElementById("predictedStock").hidden = true;
}

function mealOrd() {
    document.getElementById("stock").hidden = true;
    document.getElementById("mealOrd").hidden = false;
    document.getElementById("orders").hidden = true;
    document.getElementById("accountInfo").hidden = true;
    document.getElementById("requests").hidden = true;
    document.getElementById("predictedStock").hidden = true;
}

function orders() {
    document.getElementById("stock").hidden = true;
    document.getElementById("mealOrd").hidden = true;
    document.getElementById("orders").hidden = false;
    document.getElementById("accountInfo").hidden = true;
    document.getElementById("requests").hidden = true;
    document.getElementById("predictedStock").hidden = true;
}

function info() {
    document.getElementById("stock").hidden = true;
    document.getElementById("orders").hidden = true;
    document.getElementById("requests").hidden = true;
    document.getElementById("accountInfo").hidden = false;
    document.getElementById("mealOrd").hidden = true;
    document.getElementById("predictedStock").hidden = true;
}

function requests() {
    document.getElementById("stock").hidden = true;
    document.getElementById("mealOrd").hidden = true;
    document.getElementById("orders").hidden = true;
    document.getElementById("accountInfo").hidden = true;
    document.getElementById("requests").hidden = false;
    document.getElementById("predictedStock").hidden = true;
}

function predictedStock() {
    document.getElementById("stock").hidden = true;
    document.getElementById("mealOrd").hidden = true;
    document.getElementById("orders").hidden = true;
    document.getElementById("accountInfo").hidden = true;
    document.getElementById("requests").hidden = true;
    document.getElementById("predictedStock").hidden = false;
}

function populateMeals() {
    document.getElementById("mealList").innerHTML = '';
    var existing = []
    for(var i = 0; i < mealData.length; i++) {
        var current = mealData[i];
        if(existing.includes(current["meal_id"])) {
            continue;
        }
        existing.push(current["meal_id"])
        parent = document.getElementById("mealList");
        button = document.createElement("button")
        button.className = "w3-btn"
        button.style = "border:2px solid black"
        button.innerHTML = current["meal_name"];
        button.value = current["meal_id"];
        button.setAttribute("onclick","useMeal(this);")
        parent.appendChild(button);
    };
}

function useMeal(button){
    var mealID = button.value
    for(var i = 0; i < mealData.length; i++) {
        var current = mealData[i];
        if(current["meal_id"] == mealID) {
            var productID = current["product_id"];
            var quantity = current["weight_in_kg"]
            for(var x = 0; x < productData.length; x++) {
                var current = productData[x]
                if(current["PK_product_id"] == productID) {
                    bulkBasket.push({"variant": current["variant"], "name": current["name"], "quantity": quantity, "price": quantity * current["price_per_kg"], "id": productID});

                }
            }
        }
    }
    var table = document.getElementById("bulkOrderTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    var totalPrice = 0;
    for(var i = 0; i < bulkBasket.length; i++) {
        current = bulkBasket[i];
        totalPrice += parseInt(current["price"]);
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        cell5.innerHTML = current["id"];
        cell.innerHTML = current["variant"];
        cell1.innerHTML = current["name"];
        cell2.innerHTML = current["quantity"];
        cell3.innerHTML = current["price"];
        cell4.innerHTML = "<button type='button' class='w3-btn w3-red' onClick='removeItem(this)'>X</button>"
        cell5.innerHTML = "<div style='display: none;'>" + current["id"] + "</td>";
    }
    document.getElementById("bulkTotalPrice").value = totalPrice
}



function populatePredictedStock() {
    var table = document.getElementById("predictedStockTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < predictedStockData.length; i++) {
        current = predictedStockData[i];
        var date = formatDate(new Date(current["date_predicted"]))
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        cell.innerHTML = current["product_id"];
        cell1.innerHTML = current["name"];
        cell2.innerHTML = current["weight_in_kg"];
        cell3.innerHTML = date;
    }
}

function populateOrders() {
    if(orderData[0]["supplier"] == true) {
        var table = document.getElementById("orderTable");
        while(table.rows.length > 1) {
            table.deleteRow(1)
        }
        for(var i = 0; i < orderData.length; i++) {
            current = orderData[i];
            var row = table.insertRow(1);
            var cell = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            var cell5 = row.insertCell(5);
            var cell6 = row.insertCell(6);
            cell.innerHTML = current["PK_order_id"];
            cell1.innerHTML = current["request_id"];
            cell2.innerHTML = current["name"];
            cell3.innerHTML = current["customer_email"];
            cell4.innerHTML = current["weight_in_kg"];
            cell5.innerHTML = current["total_price"];
            cell6.innerHTML = current["date_required"];
        }
    } else {
        var table = document.getElementById("orderTable");
        while(table.rows.length > 1) {
            table.deleteRow(1)
        }
        for(var i = 0; i < orderData.length; i++) {
            current = orderData[i];
            var row = table.insertRow(1);
            var cell = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            var cell5 = row.insertCell(5);
            var cell6 = row.insertCell(6);
            cell.innerHTML = current["PK_order_id"];
            cell1.innerHTML = current["request_id"];
            cell2.innerHTML = current["name"];
            cell3.innerHTML = current["supplier_email"];
            cell4.innerHTML = current["weight_in_kg"];
            cell5.innerHTML = current["total_price"];
            cell6.innerHTML = current["date_required"];
        }
    }
}

function populateRequests(){
    var table = document.getElementById("requestTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < requestData.length; i++) {
        current = requestData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        var cell6 = row.insertCell(6);
        var cell7 = row.insertCell(7)
        cell.innerHTML = current["PK_request_id"];
        cell1.innerHTML = current["product_id"];
        cell2.innerHTML = current["name"];
        cell3.innerHTML = current["weight_in_kg"];
        cell4.innerHTML = current["date"];
        cell5.innerHTML = current["weight_in_kg"] * current["price_per_kg"];
        if(current["fulfilled"] == null || current["fulfilled"] == 0) {
            cell6.innerHTML = "No"
        } else {
            cell6.innerHTML = "Yes"
        }
        cell7.innerHTML = "<button type='button' class='w3-btn w3-green' onclick='repeatOrder(this)'>&#10004;</button>"
    }
}

function repeatOrder(x) {
    var productID = x.parentElement.parentElement.cells[1].innerHTML;
    var quantity = x.parentElement.parentElement.cells[3].innerHTML;
    var productName = x.parentElement.parentElement.cells[2].innerHTML;
    var price = x.parentElement.parentElement.cells[5].innerHTML;
    addItem(productID, quantity, productName,  price)


}

function populateInfo() {
    var name = userData["name"].split(" ");
    var address = userData["address"].split(", ")
    document.getElementById("firstName").value = name[0];
    document.getElementById("lastName").value = name[1];
    document.getElementById("email").value = userData["email"];
    document.getElementById("phoneNo").value = userData["phone_number"];
    document.getElementById("businessName").value = userData["business_name"];
    document.getElementById("addressLine1").value = address[0];
    document.getElementById("addressLine2").value = address[1];
    document.getElementById("postcode").value = address[2];
    document.getElementById("city").value = address[3];
    document.getElementById("county").value = address[4];
}


function updateStockType() {
    var typeList = []
    var x = 0;
    for(var i = 0; i < productData.length; i++){
        var opt = productData[i];
        if(typeList.includes(opt["type"])) { 
            continue;
        }
        x++;
        typeList.push(opt["type"])
        var type = document.getElementById("stockType");
        var option = document.createElement("option");
        option.text = opt["type"];
        option.value = x;
        type.add(option);
        if(userData["user_type"] == 3) {
            var bulkType = document.getElementById("bulkStockType");
            var option = document.createElement("option");
            option.text = opt["type"];
            option.value = x;
            bulkType.add(option)
        }
    }
}



function updateStockVariant() {
    // Single Orders
    if (document.getElementById("stockType").value != 0) {
        document.getElementById("stockVariant").disabled = false;
        var x = 1;
        var variantList = []
        var type = document.getElementById("stockType");
        type = type.options[type.selectedIndex].text;
        document.getElementById("stockVariant").options.length = 1;
        document.getElementById("stockName").options.length = 1;
        document.getElementById("pricePerK").value = null;
        for(var i = 0; i< productData.length; i++) {
            var opt = productData[i];
            var variant = document.getElementById("stockVariant")
            var option = document.createElement("option");
            if(opt["type"] === type) {
                if(variantList.includes(opt["variant"])){
                    continue;
                }
                variantList.push(opt["variant"]);
                option.text = opt["variant"];
                option.value = x;
                variant.add(option);
                x++;
            }
        }
    } else {
        document.getElementById("stockVariant").disabled = true;
    }
    // Bulk Orders
    if(userData["user_type"] == 3) {
        if (document.getElementById("bulkStockType").value != 0) {
            document.getElementById("bulkStockVariant").disabled = false;
            var x = 1;
            var variantList = []
            var type = document.getElementById("bulkStockType");
            type = type.options[type.selectedIndex].text;
            document.getElementById("bulkStockVariant").options.length = 1;
            document.getElementById("bulkStockName").options.length = 1;
            document.getElementById("bulkPricePerK").value = null;
            for(var i = 0; i< productData.length; i++) {
                var opt = productData[i];
                var variant = document.getElementById("bulkStockVariant")
                var option = document.createElement("option");
                if(opt["type"] === type) {
                    if(variantList.includes(opt["variant"])){
                        continue;
                    }
                    variantList.push(opt["variant"]);
                    option.text = opt["variant"];
                    option.value = x;
                    variant.add(option);
                    x++;
                }
            }
        } else {
            document.getElementById("bulkStockVariant").disabled = true;
        }
    }
}



function updateStockName() {
    // Single Orders
    if (document.getElementById("stockVariant").value != 0) {
        document.getElementById("stockName").disabled = false;
        var x = 1;
        var name = document.getElementById("stockName");
        var option = document.createElement("option");
        var variant = document.getElementById("stockVariant");
        variant = variant.options[variant.selectedIndex].text;
        document.getElementById("stockName").options.length = 1;
        document.getElementById("pricePerK").value = null;
        for(var i = 0; i < productData.length; i++) {
            var opt = productData[i];
            var name = document.getElementById("stockName");
            var option = document.createElement("option");
            if(opt["variant"] === variant) {
                option.text = opt["name"];
                option.value = x;
                name.add(option);
            }
        }
    } else {
        document.getElementById("stockName").disabled = true;
    }
    //Bulk Orders
    if(userData["user_type"] == 3) {
        if (document.getElementById("bulkStockVariant").value != 0) {
            document.getElementById("bulkStockName").disabled = false;
            var x = 1;
            var name = document.getElementById("bulkStockName");
            var option = document.createElement("option");
            var variant = document.getElementById("bulkStockVariant");
            variant = variant.options[variant.selectedIndex].text;
            document.getElementById("bulkStockName").options.length = 1;
            document.getElementById("bulkPricePerK").value = null;
            for(var i = 0; i < productData.length; i++) {
                var opt = productData[i];
                var name = document.getElementById("bulkStockName");
                var option = document.createElement("option");
                if(opt["variant"] === variant) {
                    option.text = opt["name"];
                    option.value = x;
                    name.add(option);
                }
            }
        } else {
            document.getElementById("bulkStockName").disabled = true;
        }
    }
}


function updatePricePerK() {
    if(document.getElementById("stockName").value != 0) {
        var type = document.getElementById("stockType");
        var variant = document.getElementById("stockVariant");
        var name = document.getElementById("stockName");
        type = type.options[type.selectedIndex].text;
        variant = variant.options[variant.selectedIndex].text;
        name = name.options[name.selectedIndex].text;
        document.getElementById("stockCount").disabled = false;
        for(var i = 0; productData.length; i++) {
            var opt = productData[i];
            if(opt["name"] === name && opt["type"] == type && opt["variant"] == variant) {
                document.getElementById("pricePerK").value = opt["price_per_kg"];
                break;
            }
        }
    } else {
        document.getElementById("stockCount").disabled = true;
    }
    if(userData["user_type"] == 3) {
        if(document.getElementById("bulkStockName").value != 0) {
            var name = document.getElementById("bulkStockName");
            document.getElementById("bulkStockCount").disabled = false;
            name = name.options[name.selectedIndex].text;;
            for(var i = 0; productData.length; i++) {
                var opt = productData[i];
                if(opt["name"] === name) {
                    document.getElementById("bulkPricePerK").value = opt["price_per_kg"];
                    break;
                }
            }
        } else {
            document.getElementById("bulkStockCount").disabled = true;
        }
    }
}


function updateTotalPrice() {
    if(document.getElementById("stockCount") !== "") {
        let kg = document.getElementById("pricePerK").value;
        if (kg == NaN) {
            return;
        } else {
            let amount = document.getElementById("stockCount").value;
            document.getElementById("totalPrice").value = kg * amount;
        }
    }
    if(userData["user_type"] == 3) {
        if(document.getElementById("bulkStockCount") !== "") {
            let kg = document.getElementById("bulkPricePerK").value;
            if (kg == NaN) {
                return;
            } else {
                let amount = document.getElementById("bulkStockCount").value;
                document.getElementById("bulkPrice").value = kg * amount;
                document.getElementById("bulkAddMoreButton").disabled = false;
            }
        }
    }
}

function populateStock() {
    var table = document.getElementById("stockTable")
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < stockData.length; i++) {
        current = stockData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        cell.innerHTML = current["type"];
        cell1.innerHTML = current["variant"];
        cell2.innerHTML = current["name"];
        cell3.innerHTML = current["weight_in_kg"];
    }
}

function addStock() {
    var id;
    var type = document.getElementById("stockType");
    var variant = document.getElementById("stockVariant");
    var name = document.getElementById("stockName");
    var date = document.getElementById("dateForArr").value;
    type = type.options[type.selectedIndex].text;
    variant = variant.options[variant.selectedIndex].text;
    name = name.options[name.selectedIndex].text;
    var quantity = document.getElementById("stockCount").value;
    for(var i = 0; i < productData.length; i++) {
        var opt = productData[i];
        if(opt["name"] === name && opt["type"] == type && opt["variant"] == variant) {
            id = opt["PK_product_id"]
        }
    }
    payload = {
        "hash": document.cookie,
        "id": id,
        "quantity": quantity,
        "date": date
    }
    var xhr = postRequest("addStock", payload)
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            getSupplierStock();
        } 
    }
    removeAddStock();
}

function purchase() {
    var id;
    var type = document.getElementById("stockType");
    var variant = document.getElementById("stockVariant");
    var name = document.getElementById("stockName");
    type = type.options[type.selectedIndex].text;
    variant = variant.options[variant.selectedIndex].text;
    name = name.options[name.selectedIndex].text;
    var quantity = document.getElementById("stockCount").value;
    for(var i = 0; i < productData.length; i++) {
        var opt = productData[i];
        if(opt["name"] === name && opt["type"] == type && opt["variant"] == variant) {
            id = opt["PK_product_id"]
        }
    }
    payload = {
        "hash": document.cookie,
        "id": id,
        "quantity": quantity
    }
    var xhr = postRequest("order", payload)
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.response);
            getCustomerRequests();
            getCustomerOrders();
        } 
    }
}

function enableBulk() {
    if(document.getElementById("orderName").value != "") {
        document.getElementById("bulkStockType").disabled = false;
        if(document.getElementById("bulkStockType").value != 0) {
            document.getElementById("bulkStockVariant").disabled = false;
            if(document.getElementById("bulkStockName").value != 0) {
                document.getElementById("bulkStockName").disabled = false;
                if(document.getElementById("bulkStockCount").value != "") {
                    document.getElementById("bulkStockCount").disabled = false;
                }
            }
        }
    } else {
        document.getElementById("bulkStockType").disabled = true;
        document.getElementById("bulkStockVariant").disabled = true;
        document.getElementById("bulkStockName").disabled = true;
        document.getElementById("bulkStockCount").disabled = true;
    }
}

function bulkAddMore() {
    var id;
    var totalPrice = 0;
    var type = document.getElementById("bulkStockType");
    var variant = document.getElementById("bulkStockVariant");
    var name = document.getElementById("bulkStockName");
    type = type.options[type.selectedIndex].text;
    variant = variant.options[variant.selectedIndex].text;
    name = name.options[name.selectedIndex].text;
    var amount = document.getElementById("bulkStockCount").value;
    var price = document.getElementById("bulkPrice").value;
    for(var i = 0; i < productData.length; i++) {
        var opt = productData[i];
        if(opt["name"] === name && opt["type"] == type && opt["variant"] == variant) {
            id = opt["PK_product_id"]
        }
    }
    bulkBasket.push({"variant": variant, "name": name, "quantity": amount, "price": price, "id": id});
    var table = document.getElementById("bulkOrderTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < bulkBasket.length; i++) {
        current = bulkBasket[i];
        totalPrice += parseInt(current["price"]);
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        cell5.innerHTML = current["id"];
        cell.innerHTML = current["variant"];
        cell1.innerHTML = current["name"];
        cell2.innerHTML = current["quantity"];
        cell3.innerHTML = current["price"];
        cell4.innerHTML = "<button type='button' class='w3-btn w3-red' onClick='removeItem(this)'>X</button>"
        cell5.innerHTML = "<div style='display: none;'>" + current["id"] + "</td>";
    }
    document.getElementById("bulkTotalPrice").value = totalPrice;
    document.getElementById("bulkStockCount").value = "";
    document.getElementById("bulkStockCount").disabled = true;;
    document.getElementById("bulkStockName").value = 0
    document.getElementById("bulkStockName").disabled = true;
    document.getElementById("bulkStockVariant").value = 0;
    document.getElementById("bulkStockVariant").disabled = true;
    document.getElementById("bulkStockType").value = 0;
    document.getElementById("bulkPrice").value = "";
    document.getElementById("bulkPricePerK").value = null
    document.getElementById("bulkAddMoreButton").disabled = true;

}

function removeItem(tableButton) {
    var id = tableButton.parentElement.parentElement.cells[5].childNodes[0].innerHTML;
     for(var i = 0; i < bulkBasket.length; i++) {
         var current = bulkBasket[i];
         if(current["id"] == id) {
            bulkBasket.splice(i, 1);
            break;
         }
    }
    console.log(bulkBasket)
    var table = document.getElementById("bulkOrderTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < bulkBasket.length; i++) {
        current = bulkBasket[i];
        totalPrice += parseInt(current["price"]);
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        cell5.innerHTML = current["id"];
        cell.innerHTML = current["variant"];
        cell1.innerHTML = current["name"];
        cell2.innerHTML = current["quantity"];
        cell3.innerHTML = current["price"];
        cell4.innerHTML = "<button type='button' class='w3-btn w3-red' onClick='removeItem(this)'>X</button>"
        cell5.innerHTML = "<div style='display: none;'>" + current["id"] + "</td>";
    }
    updateTotalBulkPrice();
}

function updateTotalBulkPrice() {
    var totalPrice = 0;
    for(var i = 0; i < bulkBasket.length; i++) {
        current = bulkBasket[i];
        totalPrice += parseInt(current["price"]);
    }
    document.getElementById("bulkTotalPrice").value = totalPrice;
}

function saveForLater() {
    var payload = [];
    payload.push({"name": document.getElementById("orderName").value, "hash": document.cookie})
    for(var i = 0; i < bulkBasket.length; i++) {
        current = bulkBasket[i];
        payload.push(current);
    }
    console.log(payload);
    var xhr = postRequest("saveMeal", payload);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.response);
            getMeals();

        }
    }
}

function editInfo() {
    if (document.getElementById("accountInfoTable").disabled == true) {
        document.getElementById("accountInfoTable").disabled = false
        document.getElementById("editInfo").innerHTML = "Save"
    } else {
        //sends new user data to server for update
        var error = false;
        var email = document.getElementById("email");
        var fname = document.getElementById("firstName");
        var lname = document.getElementById("lastName");
        var phoneNo = document.getElementById("phoneNo");
        var addressLine1 = document.getElementById("addressLine1");
        var addressLine2 = document.getElementById("addressLine2");
        var city = document.getElementById("city");
        var county = document.getElementById("county");
        var businessName = document.getElementById("businessName");

        email.style.borderColor = "#CCC";
        fname.style.borderColor = "#CCC";
        lname.style.borderColor = "#CCC";
        phoneNo.style.borderColor = "#CCC";
        postcode.style.borderColor = "#CCC";
        addressLine1.style.borderColor = "#CCC";
        city.style.borderColor = "#CCC";
        county.style.borderColor = "#CCC";
        businessName.style.borderColor = "#CCC";

        if(!checkEmail(email.value)){email.style.borderColor = "red"; error = true;}
        if(!checkName(fname.value, lname.value)){fname.style.borderColor = "red"; lname.style.borderColor = "red"; error = true;}
        if(!checkPhone(phoneNo.value)){phoneNo.style.borderColor = "red"; error = true;}
        if(!checkPostcode(postcode.value)){postcode.style.borderColor = "red"; error = true;}
        if(checkField(addressLine1.value)){addressLine1.style.borderColor = "red"; error = true;}
        if(checkField(city.value)){city.style.borderColor = "red"; error = true;}
        if(checkField(county.value)){county.style.borderColor = "red"; error = true;}
        if(checkField(businessName.value)){businessName.style.borderColor = "red"; error = true;}

        if(error == true){
            return;
        }

        //stick request here
        var payload = {
            "hash": document.cookie,
            "email": email.value,
            "fname": fname.value,
            "lname": lname.value,
            "phoneNo": phoneNo.value,
            "addressLine1": addressLine1.value,
            "addressLine2": addressLine2.value,
            "postcode": postcode.value,
            "city": city.value,
            "county": county.value,
            "businessName": businessName.value
        }

        var editInfoRequest = postRequest("editInfo", payload)
        editInfoRequest.onreadystatechange = function(){
            if(editInfoRequest.readyState == 4 && editInfoRequest.status == 200) {
                console.log(editInfoRequest.response);
            } 
        }

        document.getElementById("accountInfoTable").disabled = true
        document.getElementById("editInfo").innerHTML = "Edit"
    }
}

function formatDate(date) {
    var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}