var url = "http://127.0.0.1:3000"

function signUp() {
    //Sign up the user, this handles the form and then sends the data to the database
    resetBorders();
    var error = false;
    var email = document.getElementById("emailSignUp").value;
    var password = document.getElementById("password").value;
    var cnfpassword = document.getElementById("cnfpassword").value;
    var fname = document.getElementById("firstName").value;
    var lname = document.getElementById("lastName").value;
    var phoneNo = document.getElementById("phoneNo").value;
    var business = document.getElementById("businessName").value;
    var roleList = document.getElementsByName("business");
    var postcode = document.getElementById("postcode").value;
    var addressLine1 = document.getElementById("addressLine1").value;
    var addressLine2 = document.getElementById("addressLine2").value;
    var city = document.getElementById("city").value;
    var county = document.getElementById("county").value;
    var address = addressLine1 + ", " + addressLine2 + ", " + postcode + ", " + city + ", " + county;
    var specialist = document.getElementById("specialist").value;
    var orgCert = document.getElementById("organicCertificate").value;

    //Makes sure all fields are filled in
    if(!checkEmail(email)){document.getElementById("emailSignUp").style.borderColor = "red"; error = true;}
    if(!checkPassword(password)){document.getElementById("password").style.borderColor = "red"; error = true;}
    if(!checkPassword(cnfpassword)){document.getElementById("cnfpassword").style.borderColor = "red"; error = true;}
    if(!checkName(fname, lname)){document.getElementById("firstName").style.borderColor = "red"; document.getElementById("lastName").style.borderColor = "red"; error = true;}
    if(!checkPhone(phoneNo)){document.getElementById("phoneNo").style.borderColor = "red"; error = true;}
    if(checkField(business)){document.getElementById("businessName").style.borderColor = "red"; error = true;}
    if(!checkPostcode(postcode)){document.getElementById("postcode").style.borderColor = "red"; error = true;}
    if(checkField(addressLine1)){document.getElementById("addressLine1").style.borderColor = "red"; error = true;}
    if(checkField(city)){document.getElementById("city").style.borderColor = "red"; error = true;}
    if(checkField(county)){document.getElementById("county").style.borderColor = "red"; error = true;}


    //Checks that cnfpassword and password match
    if(cnfpassword != password) {
        document.getElementById("password").style.borderColor = "red";
        document.getElementById("cnfpassword").style.borderColor = "red";
        error = true;
    }

    //End the function if anything above fails and variable definitions
    if(error == true) {
        return;
    }
    if(specialist === "on") {
        specialist = true;
    } else {
        specialist = false;
    }
    for(i = 0; i < roleList.length; i++) {
        if(roleList[i].checked) {
            var role = roleList[i].value;
            if(role === "producer") {
                role = true;
            } else {
                role = false;
            }
        }
    }
    if(role == true) {
        if(!checkOrganicCertification(orgCert)){document.getElementById("organicCertificate").style.borderColor = "red"; error = true;}
        if(error == true) {
            return;
        }
    }
    

    // Payload and then sending it to the server
    payload = {
        "email": email,
        "password": password,
        "name": fname + " " + lname,
        "phoneNo": phoneNo,
        "business": business,
        "role": role,
        "address": address,
        "specialist": specialist,
        "orgCert": orgCert
    }
    console.log(payload);
    var xhr = postRequest("signup", payload)
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            //handle response
            var res = JSON.parse(xhr.response);
            if(res["signup"] == false){
                console.log("Signup Failed.")
            }
        }
    }
    document.getElementById('signUp').style.display='none'
}


function login() {
    // Handles Login as well as login form.
    document.getElementById('login').style.display='none'
    var email = document.getElementById("emailLogin").value;
    var password = document.getElementById("psw").value;
    payload = {
        "email": email,
        "password": password
    }
    var xhr = postRequest("login", payload)
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && xhr.status == 200) {
            //handle response
            console.log(xhr.response);
            var res = JSON.parse(xhr.response);
            if(res["login"] == true) {
                switch(res["user_type"]) {
                    case 1:
                        document.cookie = res["hash"];
                        window.location.replace(url + "/admin")
                        break;
                    case 2:
                        document.cookie = res["hash"];
                        window.location.replace(url + "/supplier")
                        break;
                    case 3:
                        document.cookie = res["hash"];
                        window.location.replace(url + "/customer")
                        break;
                }
            }
            else {
                console.log("Login failed");
            }
        }
    }
}

function getRequest(location, payload) {
    //Get requests
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url + "/" + location, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(payload));
    return xhr;
}

function postRequest(location, payload) {
    //Post requests
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url + "/" + location, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(payload));
    return xhr;
}

function checkField(string) {
    if(string !== "") {
        return false;
    } else {
        return true;
    }
}


function editButton(buttonId) {
    var editButton = document.getElementById(buttonId);
    if (editButton.innerHTML == "Edit") {
        editButton.innerHTML = "Save";
    } else {
        editButton.innerHTML = "Edit";
    }
}


function resetBorders(){
    //Resets all the borders of the text boxes back to nroaml colour
    document.getElementById("emailSignUp").style.borderColor = "#CCC"
    document.getElementById("password").style.borderColor = "#CCC"
    document.getElementById("cnfpassword").style.borderColor = "#CCC"
    document.getElementById("firstName").style.borderColor = "#CCC"
    document.getElementById("lastName").style.borderColor = "#CCC"
    document.getElementById("phoneNo").style.borderColor = "#CCC"
    document.getElementById("businessName").style.borderColor = "#CCC"
    document.getElementById("postcode").style.borderColor = "#CCC"
    document.getElementById("addressLine1").style.borderColor = "#CCC"
    document.getElementById("city").style.borderColor = "#CCC"
    document.getElementById("county").style.borderColor = "#CCC"
    document.getElementById("organicCertificate").style.borderColor = "#CCC"
}

function removeLogin() { document.getElementById('login').style.display='none' }
function removeSignUp() { document.getElementById('signUp').style.display='none' }
function displayLogin(){ document.getElementById('login').style.display='block' }
function displaySignup() { document.getElementById('signUp').style.display = 'block' }
function displayAddStock() { document.getElementById('addStock').style.display = 'block' }
function removeAddStock() { document.getElementById('addStock').style.display = 'none' }

function searchTable(tableName,searchRow) {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById(tableName);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[searchRow];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


function supplier() {
    document.getElementById("suppliers").hidden = false;
    document.getElementById("customers").hidden = true;
    document.getElementById("allOrders").hidden = true;
    document.getElementById("allStock").hidden = true;
}
function customer() {
    document.getElementById("suppliers").hidden = true;
    document.getElementById("customers").hidden = false;
    document.getElementById("allOrders").hidden = true;
    document.getElementById("allStock").hidden = true;
}
function allOrders() {
    document.getElementById("suppliers").hidden = true;
    document.getElementById("customers").hidden = true;
    document.getElementById("allOrders").hidden = false;
    document.getElementById("allStock").hidden = true;
}

function recvCode() {
    document.getElementById("recvCode").hidden = false;
}

function allStock() {
    document.getElementById("suppliers").hidden = true;
    document.getElementById("customers").hidden = true;
    document.getElementById("allOrders").hidden = true;
    document.getElementById("allStock").hidden = false;
}
