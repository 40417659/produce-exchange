function getAdminData() {
    var payload = {
        "hash": document.cookie
    }

    //Get Customers
    var getCustomersRequest = postRequest("getCustomers", payload) 
    getCustomersRequest.onreadystatechange = function() {
        if(getCustomersRequest.readyState == 4 && getCustomersRequest.status == 200) {
            window.customerData = JSON.parse(getCustomersRequest.response);
            console.log(customerData)
            if(customerData["supplier"] == true) { window.location.href = "/supplier"; return }
            if(customerData["supplier"] == false) { window.location.href = "/customer"; return }
            populateCustomers()
        }
    }

    //get Businesses
    var getSuppliersRequest = postRequest("getSuppliers", payload)
    getSuppliersRequest.onreadystatechange = function() {
        if(getSuppliersRequest.readyState == 4 && getSuppliersRequest.status == 200) {
            window.supplierData = JSON.parse(getSuppliersRequest.response);
            populateBusinesses();
        }
    }

    //Get Orders
    var getOrdersRequest = postRequest("getOrders", payload)
    getOrdersRequest.onreadystatechange = function() {
        if(getOrdersRequest.readyState == 4 && getOrdersRequest.status == 200) {
            window.orderData = JSON.parse(getOrdersRequest.response);
            populateOrders();
        }
    }

    //Get Stock
    var getStockRequest = postRequest("getStock", payload)
    getStockRequest.onreadystatechange = function() {
        if(getStockRequest.readyState == 4 && getStockRequest.status == 200) {
            window.stockData = JSON.parse(getStockRequest.response);
            populateStockData();
            console.log(stockData)
        }
    }
}

function populateStockData() {
    var table = document.getElementById("stockTable")
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < stockData.length; i++) {
        current = stockData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        cell.innerHTML = current["name"];
        cell1.innerHTML = current["email"];
        cell2.innerHTML = current["business_name"];
        cell3.innerHTML = current["product_name"];
        cell4.innerHTML = current["weight_in_kg"];
    }
}

function populateCustomers() {
    var table = document.getElementById("customerTable")
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < customerData.length; i++) {
        current = customerData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        cell.innerHTML = current["name"];
        cell1.innerHTML = current["email"];
        cell2.innerHTML = current["business_name"];
        cell3.innerHTML = current["phone_number"];
        cell4.innerHTML = current["address"];
        cell5.innerHTML = "<button type='button' class='w3-btn w3-red' onClick='deleteUser(this)'>Delete</button>";
    }
}

function populateBusinesses() {
    var table = document.getElementById("supplierTable")
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < supplierData.length; i++) {
        current = supplierData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        var cell6 = row.insertCell(6);
        var cell7 = row.insertCell(7);
        cell.innerHTML = current["name"];
        cell1.innerHTML = current["email"];
        cell2.innerHTML = current["cert_code"];
        cell3.innerHTML = current["business_name"];
        cell4.innerHTML = current["address"];
        cell5.innerHTML = current["phone_number"]
        cell7.innerHTML = "<button type='button' class='w3-btn w3-green' onClick='approveUser(this)'>Approve</button><button type='button' class='w3-btn w3-red' onClick='deleteUser(this)'>Delete</button>";
        if(current["approved"] == 1) {
            cell6.innerHTML = "Yes";
            cell7.innerHTML = "<button type='button' class='w3-btn w3-green' style='color: grey;' onClick='approveUser(this)' disabled>Approve</button><button type='button' class='w3-btn w3-red' onClick='deleteUser(this)'>Delete</button>";
        } else {
            cell6.innerHTML = "No"
        }
    }
}

function populateOrders() {
    var table = document.getElementById("ordersTable")
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    for(var i = 0; i < orderData.length; i++) {
        current = orderData[i];
        var row = table.insertRow(1);
        var cell = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        var cell5 = row.insertCell(5);
        var cell6 = row.insertCell(6);
        var cell7 = row.insertCell(7);
        var cell8 = row.insertCell(8);
        cell.innerHTML = current["PK_order_id"];
        cell1.innerHTML = current["request_id"];
        cell2.innerHTML = current["customer_email"];
        cell3.innerHTML = current["supplier_email"];
        cell4.innerHTML = current["product_id"];
        cell5.innerHTML = current["weight_in_kg"];
        cell6.innerHTML = current["total_price"];
        cell7.innerHTML = current["date_ordered"];
        cell8.innerHTML = current["date_required"];
    }
}

function approveUser(tableButton) {
    //get email from button
    var email = tableButton.parentElement.parentElement.cells[1].innerHTML;
    //send request to delete user with email and hash
    var payload = {
        "hash": document.cookie,
        "email": email
    }
    var approveUserRequest = postRequest("approveUser", payload) 
    approveUserRequest.onreadystatechange = function() {
       if(approveUserRequest.readyState == 4 && approveUserRequest.status == 200) {
           console.log(approveUserRequest.response);
       }
    }
    getAdminData();
}

function deleteUser(tableButton) {
    //get email from button
    var email = tableButton.parentElement.parentElement.cells[1].innerHTML;
    //send request to delete user with email and hash
    console.log(email)
    var payload = {
        "hash": document.cookie,
        "email": email
    }
    var deleteUserRequest = postRequest("deleteUser", payload) 
    deleteUserRequest.onreadystatechange = function() {
       if(deleteUserRequest.readyState == 4 && deleteUserRequest.status == 200) {
           console.log(deleteUserRequest.response);
       }
    }
    getAdminData();
}