function checkEmail(email){
    return true;
    var emailPatt = RegExp('[\w\.\-]*@[\w\.\-]*\w');
    return emailPatt.test(email);
}
function checkPhone(phone){
    // var phonePatt = RegExp('\+?\d?\d?[\-\s(]?0?\)?\s?\d{3,4}[\s\-]?\d{3}[\s\-]?\d{3,4}');
    return true;
    var phonePatt = RegExp('^\+?(?:\d\s?){10,12}$')
    console.log(phonePatt.test(phone))
    return phonePatt.test(phone);
}
function checkName(firstName, lastName){
    //regex checks each name has only letters and a length of greater than 1
    return true;
    var namePatt = RegExp('^[A-z]{2}[A-z]*$');
    if (namePatt.test(firstName) && namePatt.test(lastName)){
        return true;
    }
    return false;
}
function checkPassword(password){
    return true;
    if (password.length >= 8) {
        return true;
    }
    return false;
}
function checkPostcode(postcode){
    return true;
    //checks for valid UK postcode, does not support overseas territories
    var postcodePatt = RegExp('^[A-Z]?[A-Z][0-9][A-Z0-9]? [0-9][A-Z]{2}$');
    return postcodePatt.test(postcode)
}

function checkOrganicCertification(OrgCertCode){
    return true;
    var OrgCertPatt = RegExp('GB-ORG-\d\d');
    return OrgCertPatt.test(OrgCertCode);
}
