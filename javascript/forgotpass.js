function resetPass(){
    var email = document.getElementById("emailReset");
    if(!checkEmail(email.value)){
        email.style.borderColor = "red";
        return;
    }
    email.style.borderColor = "#CCC"
    var payload = {
        "email": email.value
    }
    
    var resetPassRequest = postRequest("resetPassword", payload) 
    resetPassRequest.onreadystatechange = function() {
       if(resetPassRequest.readyState == 4 && resetPassRequest.status == 200) {
           document.getElementById("resetInfo").innerText = "If that email matches an account, a reset password email has now been sent with further instructions"
       }
    }
}

function requestPassChange(){
    var currentPass = document.getElementById("curPass").value;
    var newPass = document.getElementById("psw").value;
    var confirmPass = document.getElementById("cnfPsw").value;

    var payload = {
        "hash": document.cookie,
        "currentPass": currentPass,
        "newPass": newPass,
        "confirmPass": confirmPass
    };
    var changePassRequest = postRequest("changePassword", payload) 
    changePassRequest.onreadystatechange = function() {
       if(changePassRequest.readyState == 4 && changePassRequest.status == 200) {
           return;
       }
    }
}

function showPassChange(){
    document.getElementById('passReset').style.display='block'; 
}

function hidePassChange(){
    document.getElementById('passReset').style.display='none'; 
}