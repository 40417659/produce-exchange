function addBasket(){
    var id;
    var name = document.getElementById("stockName");
    name = name.options[name.selectedIndex].text;
    var type = document.getElementById("stockType");
    type = type.options[type.selectedIndex].text;
    var variant = document.getElementById("stockVariant");
    variant = variant.options[variant.selectedIndex].text;
    var quantity = document.getElementById("stockCount").value;
    var price = document.getElementById("totalPrice").value;
    for(var i = 0; i < productData.length; i++) {
        var opt = productData[i];
        if(opt["name"] === name && opt["type"] === type && opt["variant"] === variant) {
            id = opt["PK_product_id"]
        }
    }
    var basketItem = {
        "hash": document.cookie,
        "id": id,
        "name": name,
        "quantity": quantity,
        "price": price
    };
    basketList.push(basketItem);
    document.getElementById("stockType").value = 0;
    document.getElementById("stockVariant").value = 0;
    document.getElementById("stockName").value = 0;
    document.getElementById("stockVariant").disabled = true;
    document.getElementById("stockName").disabled = true;
    document.getElementById("pricePerK").value = "";
    document.getElementById("stockCount").value = "";
    document.getElementById("stockCount").disabled = true;
    document.getElementById("totalPrice").value = "";
}

function addItem(id, quantity, name, price) {
    basketItem = {
        "hash": document.cookie,
        "id": id,
        "quantity": quantity,
        "name": name,
        "price": price
    }
    basketList.push(basketItem);
}

function openBasket() {
    if(bulkBasket.length != 0) {
        basketList = bulkBasket;
    }
    var table = document.getElementById("basketTable");
    while(table.rows.length > 1) {
        table.deleteRow(1)
    }
    if (basketList.length > 0){
        for(var i = 0; i < basketList.length; i++){
            var current = basketList[i];
            var row = table.insertRow(1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            cell1.innerHTML = current["name"];
            cell2.innerHTML = current["quantity"]
            cell3.innerHTML = current["price"]
        }
    }
    document.getElementById('basket').style.display = 'block';
}

function closeBasket() {
    document.getElementById('basket').style.display = 'none';
}

function orderBasket(){
    if(basketList.length == 0){
        alert("The basket is empty.");
        return;
    }
    for(var i = 0; i < basketList.length; i++){
        var current = basketList[i];
        var id = current["id"];
        var quantity = current["quantity"];
        payload = {
            "hash": document.cookie,
            "id": id,
            "quantity": quantity
        }
        var xhr = postRequest("order", payload)
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4 && xhr.status == 200) {
                console.log(xhr.response);
            } 
        }
    }
    getCustomerRequests();
    getCustomerOrders();
}

function getPrice() {
    var total = 0;
    for(var i = 0; i < basketList.length; i++) {
        console.log(basketList[i]["price"])
        total = total + parseInt(basketList[i]["price"]);
    }
    console.log(total);
    return total;
}